FROM ubuntu:18.04
MAINTAINER Michal Belica <devel@beli.sk>
EXPOSE 10000

# runs as UID 1000 GID 1000 inside the container

ENV VERSION 3.3.8

RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends gpg gpg-agent dirmngr \
	&& for ii in {1..10} ; do \
		gpg --keyserver hkps://hkps.pool.sks-keyservers.net --recv-keys 6694D8DE7BE8EE5631BED9502BD5824B7F9470E6 ; \
		rc=$? ; \
	   done \
	&& if [ $rc -ne 0 ] ; then exit $rc ; fi \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends curl xpra python3-pyqt5 python3-wheel python3-pip python3-setuptools libsecp256k1-0 libsecp256k1-dev python3-numpy python3-dev build-essential \
	&& curl -o /tmp/Electrum-${VERSION}.tar.gz https://download.electrum.org/${VERSION}/Electrum-${VERSION}.tar.gz \
	&& curl -o /tmp/Electrum-${VERSION}.tar.gz.asc https://download.electrum.org/${VERSION}/Electrum-${VERSION}.tar.gz.asc \
	&& gpg --verify /tmp/Electrum-${VERSION}.tar.gz.asc /tmp/Electrum-${VERSION}.tar.gz \
	&& pip3 install /tmp/Electrum-${VERSION}.tar.gz \
	&& test -f /usr/local/bin/electrum \
	&& rm -vrf /tmp/Electrum-${VERSION}.tar.gz /tmp/Electrum-${VERSION}.tar.gz.asc ${HOME}/.gnupg \
	&& apt-get purge --autoremove -y python3-wheel python3-pip python3-setuptools python3-dev build-essential libsecp256k1-dev curl gpg gpg-agent dirmngr \
	&& apt-get clean && rm -rf /var/lib/apt/lists/* \
	&& useradd -d /home/user -m user \
	&& mkdir /electrum \
	&& ln -s /electrum /home/user/.electrum \
	&& chown user:user /electrum \
	# disable starting of other programs in default xpra.conf
	&& sed -ie 's/^start-child/#start-child/' /etc/xpra/xpra.conf

USER user
ENV HOME /home/user
WORKDIR /home/user
VOLUME /electrum

CMD ["/usr/bin/xpra", "start", ":100", "--start-child=/usr/local/bin/electrum", "--bind-tcp=0.0.0.0:10000", "--daemon=no", "--notifications=no", "--mdns=no", "--pulseaudio=no", "--html=off", "--speaker=disabled", "--microphone=disabled", "--webcam=no", "--printing=no", "--dbus-launch=", "--exit-with-children"]
